/**
 * Related Products
 */
jQuery(function($) {
    $('.related-products-wrapper ul.products').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        autoplay: false,
        responsive: [
            {
                breakpoint: 1279,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    });

    /**
     * Shipping Address
     */
    $(document).on('change', '#use-same-address input', () => {
        $('div.shipping_address').hide();
    });
});

/**
 * Order Summary
 */
const summaryToggle = document.querySelector('.order-summary-toggle');

if (summaryToggle) {
    const orderReview = document.querySelector('.order-review-box');
    const summaryLabel = summaryToggle.querySelector('.summary-label');

    summaryToggle.addEventListener('click', (e) => {
        e.preventDefault();

        if (orderReview.classList.contains('open')) {
            summaryLabel.innerText = 'Show order summary';
            orderReview.classList.remove('open');
        } else {
            summaryLabel.innerText = 'Hide order summary';
            orderReview.classList.add('open');
        }
    });
}
