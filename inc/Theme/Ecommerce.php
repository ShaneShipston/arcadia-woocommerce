<?php

namespace Arcadia\Theme;

/**
 * Woocommerce Setup
 */
class Ecommerce
{
    public static function init()
    {
        self::registerSidebar();

        remove_action('woocommerce_before_shop_loop', 'woocommerce_output_all_notices', 10);
        remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);
        remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10);
        remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
        remove_action('woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');
        remove_action('woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20);

        add_action('woocommerce_checkout_shipping', 'woocommerce_checkout_payment', 30);
        add_action('after_setup_theme', [__CLASS__, 'declareSupport']);
        add_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 8);
        add_action('woocommerce_after_single_product', [__CLASS__, 'endWrapper'], 5);
        add_action('woocommerce_after_single_product', [__CLASS__, 'endWrapper'], 10);
        add_action('woocommerce_after_single_product', [__CLASS__, 'startRelatedWrapper'], 15);
        add_action('woocommerce_after_single_product', 'woocommerce_output_related_products', 20);
        add_action('woocommerce_before_main_content', 'woocommerce_output_all_notices', 40);
        add_action('woocommerce_shop_loop_item_title', [__CLASS__, 'startDetailWrapper'], 5);
        add_action('woocommerce_after_shop_loop_item_title', [__CLASS__, 'endWrapper'], 15);
        add_action('woocommerce_before_page_title', [__CLASS__, 'startTitleWrapper'], 10);
        add_action('woocommerce_before_shop_loop', [__CLASS__, 'endWrapper'], 25);
        add_action('woocommerce_checkout_shipping', [__CLASS__, 'shippingOptions'], 15);
        add_action('woocommerce_review_order_before_shipping', [__CLASS__, 'checkoutShippingHtml'], 999);
        add_action('woocommerce_cart_totals_before_shipping', [__CLASS__, 'cartShippingOptions'], 999);
        add_action('wp_enqueue_scripts', [__CLASS__, 'assets']);

        add_action('woocommerce_before_main_content', function () {
            echo '<button type="button" class="button filter-button">
                <em class="far fa-filter"></em>
                <span>Filter</span>
            </button>';
        }, 15);

        add_filter('woocommerce_breadcrumb_defaults', [__CLASS__, 'changeBreadcrumbDelimiter']);
        add_filter('woocommerce_catalog_orderby', [__CLASS__, 'tidyUpSortingOptions']);
        add_filter('woocommerce_output_related_products_args', [__CLASS__, 'increaseRelatedProducts'], 20);
        add_filter('woocommerce_add_to_cart_fragments', [__CLASS__, 'customCartFragment'], 30, 1);
        add_filter('woocommerce_breadcrumb_home_url', [__CLASS__, 'swapHomeURL']);
        add_filter('woocommerce_update_order_review_fragments', [__CLASS__, 'shippingMethodFragment'], 10, 1);
        add_filter('woocommerce_update_order_review_fragments', [__CLASS__, 'orderTotalFragment'], 10, 1);
    }

    public static function declareSupport()
    {
        add_theme_support('woocommerce');
        add_theme_support('wc-product-gallery-lightbox');
        add_theme_support('wc-product-gallery-slider');
    }

    public static function registerSidebar()
    {
        register_sidebar([
            'name' => __('Woocommerce Sidebar'),
            'id' => 'woo-side',
            'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ]);
    }

    public static function startRelatedWrapper($options)
    {
        echo '<div class="related-products-wrapper"><div class="container">';
    }

    public static function startTitleWrapper($options)
    {
        echo '<div class="page-title-wrapper">';
    }

    public static function startDetailWrapper($options)
    {
        echo '<div class="product-details">';
    }

    public static function endWrapper($options)
    {
        echo '</div>';
    }

    public static function changeBreadcrumbDelimiter($options)
    {
        $options['delimiter'] = ' &gt; ';
        return $options;
    }

    public static function tidyUpSortingOptions($options)
    {
        foreach ($options as $key => $option) {
            $options[$key] = ucfirst(str_replace('Sort by ', '', $option));
        }

        return $options;
    }

    public static function increaseRelatedProducts($options)
    {
        $options['posts_per_page'] = 4;
        $options['columns'] = 1;

        return $options;
    }

    public static function customCartFragment($fragments)
    {
        global $woocommerce;

        $cart = WC()->cart;

        ob_start(); ?>
        <a href="<?php echo wc_get_cart_url(); ?>" class="cart-link">
            <span class="cart-label"><?php _e('Cart', DOMAIN); ?></span> <?php echo $cart->get_cart_subtotal(); ?> <em class="far fa-shopping-bag"></em>
        </a>
        <?php
        $fragments['a.cart-link'] = ob_get_clean();

        return $fragments;
    }

    public static function swapHomeURL()
    {
        return get_permalink(woocommerce_get_page_id('shop'));
    }

    public static function assets()
    {
        wp_enqueue_script('arc-woo-slick', get_template_directory_uri() . '/slick/slick.min.js', [], null, true);
        wp_enqueue_script('arc-woo-woo', get_template_directory_uri() . '/js/woo.js', ['arc-woo-slick'], null, true);

        wp_enqueue_style('arc-woo-slick', get_template_directory_uri() . '/slick/slick.css');
        wp_enqueue_style('arc-woo-slick-theme', get_template_directory_uri() . '/slick/slick-theme.css');
    }

    public static function shippingOptions()
    {
        $packages = WC()->shipping()->get_packages();

        if (count($packages) > 0) {
            wc_cart_totals_shipping_html();
        } else {
            wc_get_template('checkout/checkout-shipping-empty.php', []);
        }
    }

    public static function shippingMethodFragment($data)
    {
        ob_start();

        self::shippingOptions();

        $data['.woocommerce-shipping-totals'] = ob_get_clean();

        return $data;
    }

    public static function cartShippingOptions()
    {
        self::shippingTemplate('cart/old-shipping.php');
    }

    public static function checkoutShippingHtml()
    {
        self::shippingTemplate('checkout/checkout-shipping.php');
    }

    public static function shippingTemplate($view)
    {
        $packages = WC()->shipping()->get_packages();
        $first    = true;

        foreach ($packages as $i => $package) {
            $chosen_method = isset(WC()->session->chosen_shipping_methods[ $i ]) ? WC()->session->chosen_shipping_methods[ $i ] : '';
            $product_names = array();

            if (count($packages) > 1) {
                foreach ($package['contents'] as $item_id => $values) {
                    $product_names[ $item_id ] = $values['data']->get_name() . ' &times;' . $values['quantity'];
                }

                $product_names = apply_filters('woocommerce_shipping_package_details_array', $product_names, $package);
            }

            wc_get_template($view, [
                'package' => $package,
                'available_methods' => $package['rates'],
                'show_package_details' => count($packages) > 1,
                'show_shipping_calculator' => is_cart() && apply_filters('woocommerce_shipping_show_shipping_calculator', $first, $i, $package),
                'package_details' => implode(', ', $product_names),
                'package_name' => apply_filters('woocommerce_shipping_package_name', (($i + 1) > 1) ? sprintf(_x('Shipping %d', 'shipping packages', 'woocommerce'), ($i + 1)) : _x('Shipping', 'shipping packages', 'woocommerce'), $i, $package),
                'index' => $i,
                'chosen_method' => $chosen_method,
                'formatted_destination' => WC()->countries->get_formatted_address($package['destination'], ', '),
                'has_calculated_shipping' => WC()->customer->has_calculated_shipping(),
            ]);

            $first = false;
        }
    }

    public static function orderTotalFragment($data)
    {
        ob_start();

        echo '<div class="order-summary-amount">';
        wc_cart_totals_order_total_html();
        echo '</div>';

        $data['.order-summary-amount'] = ob_get_clean();

        return $data;
    }
}
