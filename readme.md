# Arcadia - Woocommerce

## Setup

Add to `inc/Setup.php`

```php
// top of file
use Arcadia\Theme\Ecommerce;

// within the init() method
Ecommerce::init();
```

Add to `src/scss/style.scss`

```css

// after the banner imports
@import "woocommerce/container";
@import "woocommerce/breadcrumbs";
@import "woocommerce/catalog";
@import "woocommerce/product";
@import "woocommerce/cart";
@import "woocommerce/checkout";
@import "woocommerce/confirmation";
```

Replace the contents of `sidebar.php` with

```html
<aside class="sidebar">
    <?php dynamic_sidebar('woo-side'); ?>
</aside>
```

After everything has been installed go to Appearance > Customize from within the WordPress dashboard. Within the Woocommerce settings, change the products per column to 3.

To display the mini cart popout you will also need to add the following block of HTML into `header.php`

```html
<?php $cart = WC()->cart; ?>
<div class="cart-box">
    <a href="<?php echo wc_get_cart_url(); ?>" class="cart-link">
        <span class="cart-label"><?php _e('Cart', DOMAIN); ?></span> <?php echo $cart->get_cart_subtotal(); ?> <em class="far fa-shopping-bag"></em>
    </a>

    <div class="mini-cart-popout">
        <div class="widget_shopping_cart_content">
            <?php woocommerce_mini_cart(); ?>
        </div>
    </div>
</div>
```

If you modify the HTML structure you will also need to tweak the `customCartFragment` method in `Ecommerce.php`
