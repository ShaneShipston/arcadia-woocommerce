<div class="woocommerce-shipping-totals">
    <h3><?php _e('Shipping Method'); ?></h3>
    <div data-title="" class="shipping-options empty">
        <?php _e('Enter your shipping address to see shipping options', DOMAIN); ?>
    </div>
</div>
